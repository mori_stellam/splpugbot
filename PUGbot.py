import logging
from bot import dcclient
from bot import events as bot_events
from bot import commands as bot_commands
from dctoken import *



# """ Set up logging """
# Formats
logformat = "%(asctime)s :: %(levelname)s :: %(message)s"
datefmt = '%d/%m/%Y %H:%M:%S'
logformatter = logging.Formatter(fmt=logformat, datefmt=datefmt)
# Handlers
logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handlerF = logging.FileHandler(filename='logs/discord.log', encoding='utf-8', mode='w')
handlerF.setFormatter(logformatter)
logger.addHandler(handlerF)


# """ Setting up bot"""
discordBot = dcclient.dcbot

# Load cogs
InitCogs = [bot_events.EventsCog(discordBot),
            bot_commands.CommandsCog(discordBot)]
for cog in InitCogs:
    discordBot.load_extension(cog.name)
    discordBot.reload_extension(cog.name)

discordBot.run(TOKEN)
