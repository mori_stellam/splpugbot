import asyncio
import logging
from bot import embeds as boeds
from bot import utilities as botutils
from modules import config as pconfig
from modules import dbhandler
from discord import Interaction
from discord.ext import commands, tasks

logger = logging.getLogger('discord')


class EventsCog(commands.Cog):
    """ Cog for events. """

    def __init__(self, bot):
        self.bot = bot
        self.name = "bot.events"

    # def cog_unload(self):
    #     self.CheckEventStatus.cancel()
    #     pass

    # @tasks.loop(seconds=30)
    # async def CheckEventStatus(self):
    #     await botutils.checkEventStatus(self.bot)
    #     return None

    @commands.Cog.listener()
    async def on_ready(self):

        # Print server information
        print(f"Connected to {self.bot.get_guild(pconfig.GuildID).name}")
        for dcuser in self.bot.get_guild(pconfig.GuildID).members:
            if dcuser.id!=self.bot.user.id:
                print(f"Member: {dcuser.name}; ID {dcuser.id}")

        print("Bot ready.")


    # @commands.Cog.listener()
    # async def on_interaction(self, interact: Interaction):
    #     """ Evaluate if bot has to react to the command."""
    #     pass




def setup(bot):
    bot.add_cog(EventsCog(bot))
