import asyncio
import logging
import discord
from bot import embeds as boeds
from bot import utilities as botutils
from discord.commands import Option, slash_command
from discord.ext import commands
from modules import config as pconfig
from modules import classes as pclasses

logger = logging.getLogger('discord')
GUILDID = pconfig.GuildID
TIMESLEEP = pconfig.timeSleep

class CommandsCog(commands.Cog):
    """ Cogs for commands from discord. """

    def __init__(self, abot):
        self.bot = abot
        self.name = "bot.commands"
        self.queues = botutils.initExistingQueues()


    # -----------------------------------
    # Commands to initialize admin role
    # -----------------------------------
    @slash_command(guild_ids=[GUILDID])
    async def addmodrole(self, ctx: discord.ApplicationContext,
                therole: Option(discord.Role, "Select a role.")):
        """ Command to add discord roles as bot mod. """
        logger.info(">>> Request to initialize or change admin role.")

        if (pconfig.AdminRole<0 and ctx.guild.id==GUILDID):
            pconfig.AdminRole = therole.id
            pconfig.parsedParams["DCSERVER"]["AdminRole"] = str(therole.id)
            with open('config.ini', 'w') as cfgfile:
                pconfig.parsedParams.write(cfgfile)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Admin role initialized."))

        elif pconfig.AdminRole>0 and ctx.guild.id==GUILDID:
            if pconfig.AdminRole in [role.id for role in ctx.author.roles]:
                pconfig.AdminRole = therole.id
                pconfig.parsedParams["DCSERVER"]["AdminRole"] = str(therole.id)
                with open('config.ini', 'w') as cfgfile:
                    pconfig.parsedParams.write(cfgfile)
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Admin role changed."))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "AdminRole can only changed by admin."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Wrong server."))

        await asyncio.sleep(TIMESLEEP)
        await theMsg.delete()
        return None


    # ------------------------------
    # Manage queues
    # ------------------------------
    @slash_command(guild_ids=[GUILDID])
    async def startqueue(self, ctx: discord.ApplicationContext,
                    numplayers: Option(int, "# players to start a pug")=pconfig.defaultPugPlayers):
        """ Command to start a new queue in the channel. """
        logger.info(">>> Request to initialize a new queue in channel {ctx.channel.id}.")
        if pconfig.AdminRole<0 | (pconfig.AdminRole>0 and botutils.checkUserAdmin(ctx.user)):
            self.queues[ctx.channel.id] = pclasses.Queue(ctx.channel.id, numplayers)
            botutils.dumpQueue(self.queues)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "New queue has been started."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Only an admn can start a new queue."))
            await asyncio.sleep(TIMESLEEP)
            await theMsg.delete()
        return None


    @slash_command(guild_ids=[GUILDID])
    async def removequeue(self, ctx: discord.ApplicationContext):
        """ Command to start a remove queue in the channel. """
        logger.info(">>> Request to remove a queue in channel {ctx.channel.id}.")
        if pconfig.AdminRole<0 | (pconfig.AdminRole>0 and botutils.checkUserAdmin(ctx.user)):
            self.queues.pop(ctx.channel.id)
            botutils.dumpQueue(self.queues)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Channel has been removed from queues."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Only an admn can stop a queue."))
            await asyncio.sleep(TIMESLEEP)
            await theMsg.delete()
        return None

    
    # --------------------------------
    # Player join or leave queue
    # --------------------------------
    @slash_command(guild_ids=[GUILDID])
    async def join(self, ctx: discord.ApplicationContext):
        """ Command to join the queue if there is. """
        logger.info(">>> Request to join a queue by {ctx.user.display_name} in channel {ctx.channel.id}.")
        if ctx.channel.id in self.queues.keys():
            [returnValue, queues] = await botutils.playerjoined(ctx, self.queues)
            self.queues = queues
        else:
            await ctx.respond("There is no active queue in the channel", ephemeral=True)


def setup(bot):
    bot.add_cog(CommandsCog(bot))
