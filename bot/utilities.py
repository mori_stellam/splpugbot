import asyncio
import discord
import os
from modules import config as pconfig
from modules import classes as pclasses

PATHTOQUEUES = 'var/PugQueues.txt'


def checkUserAdmin(theuser: discord.Member):
    flag = False
    if pconfig.AdminRole<0:
        flag = True
    else:
        if pconfig.AdminRole>0 and (pconfig.AdminRole in [p.id for p in theuser.roles]):
            flag = True
    return flag


# ---------------------------------
# Functions to handle queue storage
# ---------------------------------
def initExistingQueues():
    """ Read the list of queues from textfile. """
    returnQueue = {}
    filename = os.path.relpath(PATHTOQUEUES)
    if os.path.exists(filename):
        with open(PATHTOQUEUES, 'r') as file:
            Lines = file.readlines()
            for line in Lines:
                parsed = line.strip().split(";")
                returnQueue[int(parsed[0])] = pclasses.Queue(line)
    else:
        with open(PATHTOQUEUES, 'w') as file:
            pass
    return returnQueue

def dumpQueue(queues: dict):
    """ Dump queues to textfile for permanent storage. """
    with open(PATHTOQUEUES, "w") as file:
        for key in queues.keys():
            file.write(f"{queues[key].getDumpString()}\n")
    return None


# --------------------------------
# Queue management function
# --------------------------------
async def join(ctx: discord.ApplicationContext, queue: dict):
    """ Handles a player joining a queue. """
    return [100, queue]
    

