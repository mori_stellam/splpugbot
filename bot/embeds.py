import asyncio
import discord
from datetime import datetime
from discord.ext import commands
from modules import classes, dbhandler
from modules import config as pconfig
from zoneinfo import ZoneInfo

repoUrl = pconfig.embedUrls

emojiAcc = '✅' # b'\\u2705'
emojiMay = "❔" # b'\\u2754'
emojiRej = '✖️' # b'\\u274c'
emojiSub = ""

# ----------------------------------
# Embeds
# ----------------------------------
def getSingleEmbed(lbot: commands.Bot, msg: str):
    theEmbed = discord.Embed(title="Feedback", url=repoUrl, description=msg)
    theEmbed.set_author(name=lbot.user.name, url=repoUrl, icon_url=lbot.user.avatar.url)
    return theEmbed
