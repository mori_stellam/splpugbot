import configparser

parsedParams = configparser.ConfigParser()
parsedParams.read('config.ini')


# Database and bot config
dbname = str(parsedParams["BOT"]["dbname"])
datetimeFmt = str(parsedParams["BOT"]["datetimeFmt"])
embedUrls = str(parsedParams["BOT"]["embedUrl"])
promoteWait = int(parsedParams["BOT"]["promoteWait"])
timeSleep = int(parsedParams["BOT"]["timeSleep"])
defaultPugPlayers = int(parsedParams["BOT"]["defaultPugPlayers"])

# Discord
GuildID = int(parsedParams["DCSERVER"]["DCServerID"])
AdminRole = int(parsedParams["DCSERVER"]["AdminRole"])