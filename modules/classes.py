import logging
from datetime import datetime
from modules import config as botconfig

logger = logging.getLogger('discord')


class Queue:
    def __init__(self, *args):
        if len(args)==1 and isinstance(args[0], str):
            values = args[0].split(";")
            self.constructor(*values)
        elif len(args)>=2:
            self.constructor(*args)
        else:
            logging.error("Error in QUEUE constructor: Mismatching arguments.")
        return None

    def constructor(self, channelid: int, pugsize: int=botconfig.defaultPugPlayers,
                    messageid: int=0, queue: list=[]):
        self.channelid = channelid
        self.pugsize = pugsize
        self.messageid = messageid
        self.queue = queue
        return None

    def getDumpString(self):
        return f"{self.channelid};{self.pugsize}"


class Pug:
    def __init__(self, *args):
        if len(args)==1 and isinstance(args[0], dict):
            values = [v for k, v in args[0].items()]
            self.constructor(*values)
        elif len(args)>=4:
            self.constructor(*args)
        else:
            logging.error("Error in PUG constructor: Mismatching arguments.")
        return None

    def constructor(self, id: int, team1: str, team2: str,
                    map: str, queue: int, messageid: int,
                    status: int=0):
        self.pugid = id
        self.team1 = team1
        self.team2 = team2
        self.map = map
        self.queue = queue
        self.messageid = messageid
        self.status = status

    def getQueryStr(self):
        """ Return query string of object
        """
        keys = "(" + ','.join([key for key in self.__dict__.keys()]) +  ")"
        values = "(" + \
                ','.join(["'"+str(key)+"'" for key in self.__dict__.values()]) + \
                ")"
        return [keys, values]


