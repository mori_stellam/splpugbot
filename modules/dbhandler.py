import aiosqlite
import asyncio
import functools
import logging
import os
from modules import config as pconfig
from modules import classes as pclasses
from datetime import datetime
from contextlib import asynccontextmanager


logger = logging.getLogger('discord')


# Functions to establish connection with database
async def dbinit_initTables(con: aiosqlite.Connection):
    """ Function to create the tables to the newly initialized db
    """

    await con.execute(""" CREATE TABLE pugs
                        (pugid integer, team1 text, team2, text,
                        map text, queue integer, messageid integer,
                        status integer)
                    """)
    await con.commit()
    return None

def dbdict_factory(cursor: aiosqlite.Cursor, row):
    """ Support function for dbinit_connection to return dictionaries from queries
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

async def dbinit_connection(name: str, reinit: bool=False):
    """ Function to return sqlite connection
    """
    if reinit==True:
        try:
            os.remove(name)
        except:
            pass
    flag = os.path.isfile(name)
    con = await aiosqlite.connect(name)
    con.row_factory = dbdict_factory
    if not flag:
        print("Init db file")
        await dbinit_initTables(con)
    return con

# DB connection decorator
@asynccontextmanager
async def dbConnectContext(name: str):
    logging.info(f"{name}")
    con = await dbinit_connection(name, reinit=False)
    try:
        yield con
    finally:
        await con.close()

def dbopenconnect_decor(f):
    @functools.wraps(f)
    async def wrapped_func(*args, **kwargs):
        async with dbConnectContext(pconfig.dbname) as conn:
            if not asyncio.iscoroutinefunction(f):
                return f(conn, *args, **kwargs)
            else:
                return await f(conn, *args, **kwargs)
    return wrapped_func


# ----------------------------------------
# Queries and updates
# ----------------------------------------
@dbopenconnect_decor
async def db_addPug(lcon, Pug: pclasses.Pug):
    lcurs = await lcon.execute(f"SELECT * FROM pugs WHERE pugid='{Pug.pugid}'")
    fetch = await lcurs.fetchall()
    if len(fetch)==0:
        [keys, values] = Pug.getQueryStr()
        await lcon.execute(f"INSERT INTO events {keys} VALUES {values};")
        await lcon.commit()
    return None

@dbopenconnect_decor
async def db_updatePug(lcon, Pug: pclasses.Pug):
    lcurs = await lcon.execute(f""" UPDATE events
                                    SET team1 = '{Pug.team1}',
                                        team2 = '{Pug.team2}',
                                        map = '{Pug.map}',
                                        queue = '{Pug.queue}',
                                        messageid = '{Pug.messageid}',
                                        status = '{Pug.status}'
                                    WHERE pugid = '{Pug.pugid}'
                                """)
    await lcon.commit()
    return None

@dbopenconnect_decor
async def db_getPug(lcon, pugid: int):
    lcurs = await lcon.execute(f"SELECT * FROM pugs WHERE pugid='{pugid}'")
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        returnPug = pclasses.Pug(fetch[0])
    else:
        returnPug = None
        logger.info(f"MSG : Could not find pug with ID {pugid}.")
    return returnPug

